Z3 Plugin for Netty
======================

## Installation

Before installing this plugin, you must first install the Z3 Theorem Prover from:
[https://github.com/Z3Prover/z3](https://github.com/Z3Prover/z3)

Be sure to compile the Java version of Z3 by following the guide on the link above.

Then compile the Z3 Plugin using ANT.

After compiling with ANT, there should be a new **z3.jar** file in
the directory.

Start **netty.jar** and click on the plugins tab and add **z3.jar**.
![spass1.png](https://bitbucket.org/repo/ga8jpE/images/4275789122-spass1.png)
![z32.png](https://bitbucket.org/repo/6BkbKb/images/323946386-z32.png)

After **restarting Netty**, you should see a new button labelled “Z3” on
the main toolbar of you proof.

![z33.png](https://bitbucket.org/repo/6BkbKb/images/1891368680-z33.png)