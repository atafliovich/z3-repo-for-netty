package z3;


import prooftool.backend.laws.Application;
import prooftool.backend.laws.Expression;
import prooftool.backend.laws.Literal;
import prooftool.backend.laws.Variable;

import com.microsoft.z3.Expr;

public class Z3Translator  {
	
	private Expression exp;
	
	public Z3Translator(Expression e) {
		this.exp = e;
	}

	public Expr toZ3(Z3Plugin plugin) throws TranslationException {
		if (this.exp instanceof Application) {
			Application app = (Application) exp;
			Z3Translator[] args = new Z3Translator[app.getArgs().length];
			
			int i=0;
			for (Expression e: app.getArgs()) {
				args[i] = new Z3Translator(e);
				i++;
			}
			
			Expr result = plugin.makeOperation(app.getFunId(), args);
			if (result == null) {
				throw new UntranslatableExpressionException(this);
			}
			
			return result;
		} else if (this.exp instanceof Variable) {
			return plugin.getVar((Variable) this.exp);
		} else if (this.exp instanceof Literal) {
			return plugin.translateLiteral((Literal) this.exp);
		}
		
		throw new UntranslatableExpressionException(this);
	}
	
	public Expression getExpr() {
		return this.exp;
	}

}
