package z3;

import java.util.HashMap;
import java.util.Map;

import com.microsoft.z3.ArithExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Expr;
import com.microsoft.z3.Sort;

abstract class TypeTranslation {

    private static Map<String, TypeTranslation> instances = new HashMap<>();

    static {
        instances.put(
            "bool",
            new TypeTranslation() {

                @Override
                protected Sort makeSort(Context ctx) {
                    return ctx.getBoolSort();
                }

            });

        instances.put(
            "nat",
            new TypeTranslation() {

                @Override
                protected Sort makeSort(Context ctx) {
                    return ctx.getIntSort();
                }

                @Override
                protected BoolExpr makeConstraint(Context ctx, Expr var) {
                    return ctx.mkGe((ArithExpr)var, ctx.mkInt(0));
                }

            });

        instances.put(
            "int",
            new TypeTranslation() {

                @Override
                protected Sort makeSort(Context ctx) {
                    return ctx.getIntSort();
                }
            
            });
    }

    /**
     * Convert a Netty variable into an intermediate variable.
     *
     * @param ctx Z3 context to use
     * @param name the name of the variable
     * @param typeName the name of the (Netty) type, eg. "bool" or "nat"
     * @return an intermediate variable of the appropriate name and type with
     * constraints implied by the Netty type, or null if the type isn't
     * supported
     */
    public static IntermediateVar makeVariable(
            Context ctx,
            String name,
            String typeName) {
        TypeTranslation tt = instances.get(typeName);
        if (tt == null) return null;

        Expr var = ctx.mkConst(name, tt.makeSort(ctx));
        BoolExpr constraint = tt.makeConstraint(ctx, var);

        return new IntermediateVar(var, constraint);
    }

    private TypeTranslation() {
    }

    protected abstract Sort makeSort(Context ctx);

    protected BoolExpr makeConstraint(Context ctx, Expr var) {
        // Default to no constraint.
        return ctx.mkTrue();
    }

}
