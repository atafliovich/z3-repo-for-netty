package z3;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.microsoft.z3.ArithExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Expr;
import com.microsoft.z3.Solver;
import com.microsoft.z3.Status;

import prooftool.backend.direction.Direction;
import prooftool.backend.laws.Identifier;
import prooftool.backend.laws.Literal;
import prooftool.backend.laws.Scope;
import prooftool.backend.laws.Variable;
import prooftool.util.Identifiers;

public class Z3Plugin {

    private static Set<Identifier> genericOps = new HashSet<>();

    static {
        genericOps.add(Identifiers.eq);
        genericOps.add(Identifiers.not_eq);
        genericOps.add(Identifiers.ifthenelse);
    }

    // Note: By "boolean" operators we mean operators that act on booleans,
    // not necessarily operators that return booleans. (Similarly for number
    // ops.)

    private static Set<Identifier> boolOps = new HashSet<>();

    static {
        boolOps.add(Identifiers.and);
        boolOps.add(Identifiers.or);
        boolOps.add(Identifiers.not);
        boolOps.add(Identifiers.imp);
        boolOps.add(Identifiers.rev_imp);
    }

    private static Set<Identifier> numberOps = new HashSet<>();

    static {
        numberOps.add(Identifiers.plus);
        numberOps.add(Identifiers.times);
        numberOps.add(Identifiers.unary_minus);
        numberOps.add(Identifiers.infix_minus);
        numberOps.add(Identifiers.divide);
        numberOps.add(Identifiers.power);
        numberOps.add(Identifiers.lt);
        numberOps.add(Identifiers.gt);
        numberOps.add(Identifiers.lte);
        numberOps.add(Identifiers.gte);
    }

    private static Set<Identifier> quantifiers = new HashSet<>();

    static {
        quantifiers.add(Identifiers.forall);
        quantifiers.add(Identifiers.exists);
    }

    private Context ctx;
    private Map<Variable, Expr> vars = new HashMap<>();
    private Solver solver;

    /**
     * Creates a new Z3 plugin with a default Z3 context.
     */
    public Z3Plugin() {
        this(new Context());
    }

    /**
     * Creates a new Z3 plugin using a given Z3 context.
     *
     * @param context the Z3 context to use
     */
    public Z3Plugin(Context context) {
        ctx = context;
        solver = ctx.mkSolver();
    }

    /**
     * Adds a boolean context expression to this plugin.
     *
     * @param e A Netty object that can be translated to a Z3 boolean
     * expression
     * 
     * @throws TranslationException If context expression is not a boolean
     */
    public void addContext(Z3Translator e) throws TranslationException {
        Expr ze = e.toZ3(this);
        
        if (!(ze instanceof BoolExpr)) {
            String message = String.format("context expressions must be boolean; got `%s`", e);
            throw new TypeTranslationException(e, message);
        }
        
        solver.add((BoolExpr)ze);
    }
    
    /**
     * 
     * @return the String of the Model of this Z3Plugin
     */
    public String getModel() {
    	//needs to be called twice for some reason
    	return this.solver.getModel().toString();
    }
    
    /**
     * 
     * @return String that contains the reason why the solver couldn't
     * prove it True or False (unknown)
     */
    public String getReasonUnknown() {
    	return this.solver.getReasonUnknown();
    }

    /**
     * Attempts to prove a relation between two expressions.
     *
     * @param d the direction (relation) to prove
     * @param lhs the first expression
     * @param rhs the second expression
     * @return true if Z3 can prove the relation, otherwise false
     * @throws TranslationException if the expressions can't be translated
     */
    public Status canProve(Direction d, Z3Translator lhs, Z3Translator rhs) throws TranslationException {
        Expr zlhs = lhs.toZ3(this);
        Expr zrhs = rhs.toZ3(this);

        Identifier id = d.getIdent();
        BoolExpr toProve;
        if (id == Identifiers.eq) {
            toProve = ctx.mkEq(zlhs, zrhs);
        }
        else if (id == Identifiers.gte
                 && zlhs instanceof ArithExpr
                 && zrhs instanceof ArithExpr) {
            toProve = ctx.mkGe((ArithExpr)zlhs, (ArithExpr)zrhs);
        }
        else if (id == Identifiers.lte
                 && zlhs instanceof ArithExpr
                 && zrhs instanceof ArithExpr) {
            toProve = ctx.mkLe((ArithExpr)zlhs, (ArithExpr)zrhs);
        }
        else if (id == Identifiers.imp
                 && zlhs instanceof ArithExpr
                 && zrhs instanceof ArithExpr) {
            toProve = ctx.mkImplies((BoolExpr)zlhs, (BoolExpr)zrhs);
        }
        else if (id == Identifiers.rev_imp
                 && zlhs instanceof ArithExpr
                 && zrhs instanceof ArithExpr) {
            toProve = ctx.mkImplies((BoolExpr)zrhs, (BoolExpr)zlhs);
        }
        else return Status.SATISFIABLE;

        // Try to satisfy the negation.
        solver.add(ctx.mkNot(toProve));
        //boolean result = solver.check() == Status.UNSATISFIABLE;

        return solver.check();
    }

    /**
     * Resets the state of this plugin. All context will be deleted and all
     * variables will be forgotten.
     */
    public void reset() {
        solver.reset();
        vars.clear();
    }

    /**
     * Gets the Z3 variable corresponding to a given Netty variable.
     *
     * @param v the Netty variable to look up
     * @return the corresponding Z3 variable, or null if none exists
     * 
     * @throws TranslationException If the variable does not exist
     */
    public Expr getVar(Variable v) throws TranslationException {
        Expr result = vars.get(v);
        
        if (result == null) {
        	throw new UntranslatableExpressionException(new Z3Translator(v));
        }
        
        return result;
    }

    /**
     * Creates a Z3 expression representing a given Netty literal.
     *
     * @param lit the literal to translate
     * @return a Z3 expression representing this literal, or null if that
     * isn't possible
     * 
     * @throws TranslationException If the result is not able to be translated
     */
    public Expr translateLiteral(Literal lit) throws TranslationException {
        Expr result;
        String s = lit.toString();

        if (lit.equals(Literal.top)) {
        	result = ctx.mkTrue();
        } else if (lit.equals(Literal.bottom)) {
        	result = ctx.mkFalse();
        } else if (s.matches("\\d+")) {
        	result = ctx.mkInt(s);
        } else if (s.matches("\\d*\\.\\d+")) {
        	result = ctx.mkReal(s);
        } else {
            throw new UntranslatableExpressionException(new Z3Translator(lit));
        }

        return result;
    }

    /**
     * Creates a Z3 expression representing a given Netty operator applied
     * to a list of Netty arguments.
     *
     * @param op the identifier for the operator
     * @param args the arguments to the operator
     * @return a Z3 expression representing this operation, or null if that
     * isn't possible
     * 
     * @throws TranslationException If the result is not able to be translated
     */
    public Expr makeOperation(Identifier op, Z3Translator... args) throws TranslationException {
        Expr result = null;
        if (genericOps.contains(op)) {
        	result = translateGenericOp(op, args);
        } else if (boolOps.contains(op)) {
        	result = translateBoolOp(op, args);
        } else if (numberOps.contains(op)) {
        	result = translateNumberOp(op, args);
        } else if (quantifiers.contains(op)) {
            assert args.length == 1;
            result = translateQuantifier(op, (Scope) (args[0]).getExpr());
        } else if (op == Identifiers.colon) {
            assert args.length == 2;
            result = translateColon((Variable)args[0].getExpr(), args[1]);
        }
        
        // If the operator is unknown, we return null. The toZ3() method of
        // Application will throw an exception in this case.
        return result;
    }

    private Expr translateGenericOp(Identifier op, Z3Translator[] args) throws TranslationException {
        Expr[] zArgs = new Expr[args.length];

        for (int i = 0; i < args.length; ++i) {
            zArgs[i] = args[i].toZ3(this);
        }

        Expr result = null;

        if (op == Identifiers.eq) {
            assert zArgs.length == 2;
            result = ctx.mkEq(zArgs[0], zArgs[1]);
        }
        else if (op == Identifiers.not_eq) {
            assert zArgs.length == 2;
            result = ctx.mkNot(ctx.mkEq(zArgs[0], zArgs[1]));
        }
        else if (op == Identifiers.ifthenelse) {
            assert zArgs.length == 3;
            result = ctx.mkITE((BoolExpr)zArgs[0], zArgs[1], zArgs[2]);
        }

        return result;
    }

    private Expr translateBoolOp(Identifier op, Z3Translator[] args) throws TranslationException {
        BoolExpr[] zArgs = new BoolExpr[args.length];

        for (int i = 0; i < args.length; ++i) {
            Expr arg = args[i].toZ3(this);
            if (!(arg instanceof BoolExpr)) {
                String message = String.format(
                    "expected a boolean expression; got `%s`", args[i]);
                // XXX The attached expression should really be the overall
                // boolean operator and not the mistyped argument.
                throw new TypeTranslationException(args[i], message);
            }
            zArgs[i] = (BoolExpr)arg;
        }

        Expr result = null;

        if (op == Identifiers.and) result = ctx.mkAnd(zArgs);
        else if (op == Identifiers.or) result = ctx.mkOr(zArgs);
        else if (op == Identifiers.not) {
            assert zArgs.length == 1;
            result = ctx.mkNot(zArgs[0]);
        }
        else if (op == Identifiers.imp) {
            assert zArgs.length == 2;
            result = ctx.mkImplies(zArgs[0], zArgs[1]);
        }
        else if (op == Identifiers.rev_imp) {
            assert zArgs.length == 2;
            result = ctx.mkImplies(zArgs[1], zArgs[0]);
        }

        return result;
    }

    private Expr translateNumberOp(Identifier op, Z3Translator[] args) throws TranslationException {
        ArithExpr[] zArgs = new ArithExpr[args.length];

        for (int i = 0; i < args.length; ++i) {
            Expr arg = args[i].toZ3(this);
            if (!(arg instanceof ArithExpr)) {
                String message = String.format(
                    "expected an arithmetic expression; got `%s`", args[i]);
                throw new TypeTranslationException(args[i], message);
            }
            
            zArgs[i] = (ArithExpr)arg;
        }

        Expr result = null;

        if (op == Identifiers.plus) {
        	result = ctx.mkAdd(zArgs);
        } else if (op == Identifiers.times) {
        	result = ctx.mkMul(zArgs);
        } else if (op == Identifiers.unary_minus) {
            assert zArgs.length == 1;
            result = ctx.mkUnaryMinus(zArgs[0]);
        } else if (op == Identifiers.infix_minus) {
        	result = ctx.mkSub(zArgs);
        } else if (op == Identifiers.divide) {
            assert zArgs.length == 2;
            result = ctx.mkDiv(zArgs[0], zArgs[1]);
        } else if (op == Identifiers.power) {
            assert zArgs.length == 2;
            result = ctx.mkPower(zArgs[0], zArgs[1]);
        } else if (op == Identifiers.lt) {
            assert zArgs.length == 2;
            result = ctx.mkLt(zArgs[0], zArgs[1]);
        } else if (op == Identifiers.gt) {
            assert zArgs.length == 2;
            result = ctx.mkGt(zArgs[0], zArgs[1]);
        } else if (op == Identifiers.lte) {
            assert zArgs.length == 2;
            result = ctx.mkLe(zArgs[0], zArgs[1]);
        } else if (op == Identifiers.gte) {
            assert zArgs.length == 2;
            result = ctx.mkGe(zArgs[0], zArgs[1]);
        }

        return result;
    }

    private Expr translateQuantifier(Identifier quantifier, Scope scope) throws TranslationException {
        Variable dummy = scope.getDummy();
        String typeName = scope.getDomain().toString();
        IntermediateVar iv = TypeTranslation.makeVariable(ctx, dummy.toString(), typeName);
        if (iv == null) {
            String message = String.format(
                "type '%s' is not supported by the Z3 plugin",
                typeName);
            // XXX It's really the quantifier expression itself that's
            // untranslatable, not the dummy variable.
            throw new UntranslatableExpressionException(new Z3Translator(dummy), message);
        }

        vars.put(dummy, iv.getVar());

        Expr[] quantVars = {iv.getVar()};
        Z3Translator translator = new Z3Translator(scope.getBody());
        Expr body = translator.toZ3(this);
        if (!(body instanceof BoolExpr)) {
            String message = String.format("expected a boolean expression, got `%s`", body);
            throw new TypeTranslationException(translator, message);
        }

        Expr result = null;

        if (quantifier == Identifiers.forall) {
            result = ctx.mkForall(quantVars, ctx.mkImplies(iv.getConstraint(), (BoolExpr)body), 0, null, null, null, null);
        }
        else if (quantifier == Identifiers.exists) {
            result = ctx.mkExists(quantVars, ctx.mkAnd(iv.getConstraint(), (BoolExpr)body), 0, null, null, null, null);
        }

        return result;
    }

    private Expr translateColon(Variable var, Z3Translator type) throws TranslationException {
        String varName = var.toString();
        String typeName = type.toString();
        IntermediateVar iv = TypeTranslation.makeVariable(ctx, varName, typeName);
        if (iv == null) {
            String message = String.format("type '%s' is not supported by the Z3 plugin", typeName);
            // XXX As in the quantifier case, this should be called with the
            // colon expression, not the variable.
            throw new UntranslatableExpressionException(new Z3Translator(var), message);
        }

        vars.put(var, iv.getVar());
        return iv.getConstraint();
    }
}
