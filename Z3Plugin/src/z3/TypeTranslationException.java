package z3;

/**
 * Exception thrown when Z3 types of translated expressions do not match.
 */
public class TypeTranslationException extends TranslationException {

    private static final long serialVersionUID = 6564386631093431891L;

    public TypeTranslationException(Z3Translator expr) {
        super(expr);
    }

    public TypeTranslationException(Z3Translator expr, String message) {
        super(expr, message);
    }

}
