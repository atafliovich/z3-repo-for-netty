package z3;

/**
 * Exception thrown when an object cannot be translated.
 */
public class UntranslatableExpressionException extends TranslationException {

    private static final long serialVersionUID = -5354373756187731946L;
    private static final String MSG_FORMAT = "cannot translate expression `%s`";

    public UntranslatableExpressionException(Z3Translator expr) {
        this(expr, String.format(MSG_FORMAT, expr));
    }

    public UntranslatableExpressionException(Z3Translator expr, String msg) {
        super(expr, msg);
    }

}
