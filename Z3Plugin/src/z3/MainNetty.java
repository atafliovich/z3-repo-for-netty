/**
 * 
 */
package z3;

import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import plugins.NettyPlugin;
import plugins.PluginAPI;
import prooftool.backend.laws.Expression;
import prooftool.gui.prooftree.ProofTree;
import prooftool.proofrepresentation.ProofElement;
import prooftool.proofrepresentation.ProofLine;
import prooftool.proofrepresentation.justification.Justification;

import com.microsoft.z3.Status;

/**
 * @author ben
 *
 */
public class MainNetty implements NettyPlugin {
	
	public void tryZ3(ProofTree prooftree) {
		Z3Plugin z3 = new Z3Plugin();
		
		// We need to find the two most recent lines, at the current zoom level.
		// We do this as follows: if the most recent child is a Proof, zoom in.
		// Repeat until we find a proof whose most recent child is not a proof.
		// (Instead it is expected to be an input box.) If the two children before
		// that are both ProofLines, we can continue.
		PluginAPI.loadLines();
		if (PluginAPI.getNumLines() < 3) {
			JOptionPane.showMessageDialog(prooftree, "Not enough lines.");
			return;
		}
		ProofElement secondLastLine = PluginAPI.getSecondLastLine();
		ProofElement lastLine = PluginAPI.getLastLine();

		Expression lhs, rhs;

		if (!(secondLastLine instanceof ProofLine) || !(lastLine instanceof ProofLine)) {
			JOptionPane.showMessageDialog(prooftree, "Cannot use Z3 here.");
			return;
		}
		else {
			lhs = ((ProofLine)secondLastLine).getExpn();
			rhs = ((ProofLine)lastLine).getExpn();
		}

		List<List<Expression>> context = PluginAPI.getFullContext();

		try {
			for (List<Expression> le : context)
				for (Expression e : le) {
					z3.addContext(new Z3Translator(e));
				}

			//unknown bug
			Z3Translator left = new Z3Translator(lhs);
			Z3Translator right = new Z3Translator(rhs);
			
			Status result = z3.canProve(lastLine.getDirection(), left, right);
			result = z3.canProve(lastLine.getDirection(), left, right);
			if (result == Status.UNSATISFIABLE) {
				Justification j = new Justification("proved with Z3", true);
				secondLastLine.setJustification(j);
				PluginAPI.refreshLastLines();
			}
			else if (result == Status.UNKNOWN){
				JOptionPane.showMessageDialog(prooftree, "Failed to prove because: " + z3.getReasonUnknown());
			}
			else {
				String reason = z3.getModel();
				JOptionPane.showMessageDialog(prooftree, "Exists a counter example: " + reason);
			}
		}
		catch (TranslationException exc) {
			JOptionPane.showMessageDialog(prooftree, exc.getMessage());
		}
		
	}

	/* (non-Javadoc)
	 * @see plugins.NettyPlugin#run()
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub
		tryZ3(PluginAPI.prooftree);
	}

	/* (non-Javadoc)
	 * @see plugins.NettyPlugin#getPluginName()
	 */
	@Override
	public String getPluginName() {
		// TODO Auto-generated method stub
		return "Z3";
	}

	@Override
	public Icon getIcon() {
		// TODO Auto-generated method stub
		return new ImageIcon(getClass().getResource("/z3/z3icon.png"));
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return "Prove something with Z3";
	}
	
	public static void main(String[] args) {
		System.out.println("Z3 loaded!");
	}

}
