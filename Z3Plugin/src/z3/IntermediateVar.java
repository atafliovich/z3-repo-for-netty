package z3;

import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Expr;

/**
 * An intermediate representation of a variable, used to keep track of type
 * information that Z3's type system doesn't handle.
 *
 * <p>Wraps a Z3 variable and a boolean expression representing additional
 * constraints on that variable which are considered by Netty as part of the
 * variable's type, but cannot be represented as such in Z3. For instance,
 * a Netty variable of type "nat" will be represented as an integer variable
 * with a constraint that it is non-negative.</p>
 */
class IntermediateVar {

    private Expr var;
    private BoolExpr constraint;

    public IntermediateVar(Expr var, BoolExpr constraint) {
        this.var = var;
        this.constraint = constraint;
    }

    public Expr getVar() {
        return var;
    }

    public BoolExpr getConstraint() {
        return constraint;
    }

}
