package z3;

/**
 * Exception thrown for errors translating Netty objects to Z3.
 */
public class TranslationException extends Exception {

    private static final long serialVersionUID = -6400722902134109451L;

    private Z3Translator expr;

    public TranslationException(Z3Translator expr) {
        this.expr = expr;
    }

    public TranslationException(Z3Translator expr, String message) {
        super(message);
        this.expr = expr;
    }

    /**
     * @return the expression that failed to be translated
     */
    public Z3Translator getExpr() {
        return expr;
    }

}
